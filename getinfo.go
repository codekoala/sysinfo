package main

import (
	"bitbucket.org/codekoala/sysinfo/sysinfo"
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

const APP = "System Information Gatherer"

var (
	asJson      bool
	showVersion bool
)

func init() {
	sysinfo.InitPlatform()

	flag.BoolVar(&showVersion, "v", false, "Display version")
	flag.BoolVar(&asJson, "j", false, "Return JSON")
	flag.Parse()

	if showVersion {
		fmt.Println(APP, sysinfo.VERSION)
		os.Exit(0)
	}
}

func main() {
	info := sysinfo.GetAllInfo()

	if asJson {
		b, _ := json.Marshal(info)
		os.Stdout.Write(b)
		os.Exit(0)
	}

	fmt.Println("OS:\t\t", info.System.Os)
	fmt.Println("Edition:\t", info.System.Edition)
	fmt.Println("Architecture:\t", info.System.Arch)
	fmt.Println("Kernel:\t\t", info.System.Kernel)
	fmt.Println("GUID:\t\t", info.System.GUID)
	fmt.Println("System Type:\t", info.System.Type)
	fmt.Println("Locale:\t\t", info.System.Locale)

	fmt.Println("IP Address:\t", info.Network.Ip)
	fmt.Println("MAC Address:\t", info.Network.Mac)
}
