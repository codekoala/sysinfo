package sysinfo

import (
	"testing"
)

func TestIsIPv4(t *testing.T) {
	if isIPv4("hello there") {
		t.Error("Detected IPv4 address from words")
	}

	if isIPv4("1.2") {
		t.Error("Detected IPv4 address without 4 octets")
	}

	if isIPv4("0.0.0.0") {
		t.Error("0.0.0.0 is not a valid IPv4 address")
	}

	if isIPv4("0.1.2.3") {
		t.Error("0.1.2.3 is not a valid IPv4 address")
	}

	if !isIPv4("10.0.0.0") {
		t.Error("10.0.0.0 is a valid IPv4 address")
	}

	if !isIPv4("192.168.1.1") {
		t.Error("192.168.1.1 is a valid IPv4 address")
	}
}

func TestIsIPv6(t *testing.T) {
	if isIPv6("hello there") {
		t.Error("Detected IPv6 address from words")
	}

	if !isIPv6("fe80::d6be:d9ff:fe9f:47e4") {
		t.Error("fe80::d6be:d9ff:fe9f:47e4 is a valid IPv6 address")
	}
}

func TestIsIP(t *testing.T) {
	if isIP("hello there") {
		t.Error("Detected IP address from words")
	}

	if !isIP("192.168.1.1") {
		t.Error("192.168.1.1 is a valid IPv4 address")
	}

	if !isIP("fe80::d6be:d9ff:fe9f:47e4") {
		t.Error("fe80::d6be:d9ff:fe9f:47e4 is a valid IPv6 address")
	}
}

func TestOctets(t *testing.T) {
	bits := Ip{"10.20.30.40"}.Octets()
	if bits[0] != 10 || bits[1] != 20 || bits[2] != 30 || bits[3] != 40 {
		t.Error("Failed to parse IP address into octets")
	}
}

func TestFirstOctets(t *testing.T) {
	o1, o2 := Ip{"10.20.30.40"}.FirstOctets()
	if o1 != 10 || o2 != 20 {
		t.Error("Failed to parse IP address into octets")
	}
}

func TestInRange(t *testing.T) {
	begin := Ip{"192.168.128.20"}
	end := Ip{"192.168.128.30"}

	ip := Ip{"192.168.128.29"}
	if !ip.InRange(begin, end) {
		t.Error("Failed to detect IP in range")
	}

	ip = Ip{"192.168.128.31"}
	if ip.InRange(begin, end) {
		t.Error("Incorrectly detected IP in range")
	}
}
