package sysinfo

import (
	"net"
	"regexp"
)

/**
 * Placeholder for Linux stuff
 **/
func InitPlatform() {
}

/**
 * Return OS type
 **/
func GetOS() string {
	return getOutput("sysctl", "-n", "kern.ostype")
}

/**
 * Determine the version of OSX that we're dealing with.
 * TODO: Map this to a codename
 **/
func GetEdition() string {
	output := getOutput("system_profiler", "SPSoftwareDataType")
	re := regexp.MustCompile("System Version: (.*)")
	match := re.FindStringSubmatch(output)

	return match[1]
}

/**
 * Determine CPU architecture.
 **/
func GetArch() string {
	return getOutput("sysctl", "-n", "hw.machine")
}

/**
 * Determine kernel version.
 **/
func GetKernel() string {
	return getOutput("sysctl", "-n", "kern.osrelease")
}

/**
 * Get system UUID if possible.
 **/
func GetGUID() string {
	return getOutput("sysctl", "-n", "kern.uuid")
}

/**
 * Determine the type of Mac this is.
 **/
func GetSystemType() string {
	return getOutput("sysctl", "-n", "hw.model")
}

/**
 * Return the name of a network interface.
 **/
func GetNicName(iface net.Interface) string {
	return iface.Name
}
