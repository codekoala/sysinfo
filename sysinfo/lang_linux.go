package sysinfo

import (
	"os/exec"
	"strings"
)

// GetLocaleName returns the LANG value from the locale command. Returns
// "en_US.UTF-8" if any errors happen.
func GetLocaleName() string {
	out, err := exec.Command("locale").Output()
	if err == nil {
		for _, line := range strings.Split(string(out), "\n") {
			if strings.HasPrefix(line, "LANG=") {
				parts := strings.SplitN(line, "=", 2)
				return parts[1]
			}
		}
	}

	return "en_US.UTF-8"
}
