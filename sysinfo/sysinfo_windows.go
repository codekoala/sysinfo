// +build windows

package sysinfo

import (
	"fmt"
	"net"
	"os"
	"strings"
)

/**
 * Placeholder to handle Windows-specific initialization.
 **/
func InitPlatform() {
}

/**
 * Return the raw version of Windows from the registry.
 **/
func GetRawOS() (name string) {
	name, _ = GetWindowsNT("ProductName", 100)
	name = strings.TrimPrefix(name, "Microsoft")

	name = strings.Replace(name, "(TM)", "", -1)
	name = strings.Replace(name, "(R)", "", -1)
	name = strings.Replace(name, "  ", " ", -1)

	return strings.TrimSpace(name)
}

/**
 * Return the version of Windows from the registry, with edition information
 * removed.
 **/
func GetOS() (name string) {
	name = GetRawOS()

	maybe_name := strings.TrimSuffix(name, GetEdition())
	if maybe_name != "Windows " {
		name = maybe_name
	}

	return strings.TrimSpace(name)
}

/**
 * Return the Windows edition name from the registry.
 **/
func GetEdition() (edition string) {
	edition, _ = GetWindowsNT("EditionID", 100)
	edition = strings.TrimSpace(edition)

	// older versions of Windows do not include an explicit edition in the
	// registry
	if edition == "" {
		edition = strings.TrimPrefix(GetRawOS(), "Microsoft ")
		edition = strings.TrimPrefix(edition, "Windows")
	}

	// no need to include "Server" in editions
	edition = strings.TrimPrefix(edition, "Server")

	// Fix N editions
	sz := len(edition)
	if sz > 0 && edition[sz-1] == 'N' {
		edition = edition[:sz-1] + " N"
	}

	return strings.TrimSpace(edition)
}

/**
 * Attempt to determine whether it's 32-bit or 64-bit windows.
 **/
func GetArch() (arch string) {
	// compiled program's bitness
	app_arch := os.Getenv("PROCESSOR_ARCHITECTURE")

	// Windows' bitness
	os_arch := os.Getenv("PROCESSOR_ARCHITEW6432")

	arch = "x86"
	if app_arch == "AMD64" || os_arch == "AMD64" {
		arch = "x64"
	}

	return arch
}

/**
 * Return the kernel number and build number combined.
 **/
func GetKernel() string {
	ver, _ := GetWindowsNT("CurrentVersion", 100)
	build, _ := GetWindowsNT("CurrentBuildNumber", 10)

	return fmt.Sprintf("%s.%s", ver, build)
}

/**
 * Return the system's GUID.
 **/
func GetGUID() (guid string) {
	guid, _ = GetMicrosoft("Cryptography/MachineGuid", 36)
	return guid
}

/**
 * Attempt to determine whether we're running in VirtualBox, VMware, or on a
 * physical machine.
 **/
func GetSystemType() (systype string) {
	systype = "physical"

	system, _ := GetSysControl("SystemInformation/SystemProductName", 100)
	if IsVirtualBox(system) {
		systype = "VirtualBox"
	} else if IsVMware(system) {
		systype = "VMware"
	} else {
		// fallback to checking video information
		video, _ := GetSysControl("Class/{4D36E968-E325-11CE-BFC1-08002BE10318}/0000/Settings/Device Description", 100)
		if IsVirtualBox(video) {
			systype = "VirtualBox"
		} else if IsVMware(video) {
			systype = "VMware"
		}
	}

	return systype
}

/**
 * Return the name of a network interface.
 **/
func GetNicName(iface net.Interface) string {
	key := fmt.Sprintf("Network/{4D36E972-E325-11CE-BFC1-08002BE10318}/%s/Connection/Name", iface.Name)
	name, _ := GetSysControl(key, 100)

	if name == "" {
		name = iface.Name
	}

	return name
}
