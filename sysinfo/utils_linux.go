package sysinfo

import (
	"bufio"
	"os"
)

// Generic type for handling lines from a file
type handler func(string) bool
type scanHandler func(*bufio.Scanner)

/**
 * Generic function to open a file, create a scanner, do something with said
 * scanner, and close the file.
 **/
func checkFile(filename string, fn scanHandler) {
	if file, err := os.Open(filename); err == nil {
		defer file.Close()
		fn(bufio.NewScanner(file))
	}
}

/**
 * Generic function to check the first line of a file.
 **/
func CheckFirstLine(filename string, fn handler) {
	checkFile(filename, func(scanner *bufio.Scanner) {
		scanner.Scan()
		fn(scanner.Text())
	})
}

/**
 * Generic function to check all lines of a file up to the first one to "pass"
 * our check function.
 **/
func CheckLines(filename string, fn handler) {
	checkFile(filename, func(scanner *bufio.Scanner) {
		for scanner.Scan() {
			if fn(scanner.Text()) {
				break
			}
		}
	})
}
