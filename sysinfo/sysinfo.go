package sysinfo

import (
	"os"
)

const VERSION = "1.3.1"

type SystemInfo struct {
	Hostname string `json:"hostname"`
	Os       string `json:"os"`
	Edition  string `json:"edition"`
	Arch     string `json:"arch"`
	Kernel   string `json:"kernel"`
	GUID     string `json:"guid"`
	Type     string `json:"type"`
	Locale   string `json:"locale"`
}

type NetInfo struct {
	Nics []Nic  `json:"adapters"`
	Mac  string `json:"ether"`
	Ip   Ip     `json:"ipv4"`
}

type MetaInfo struct {
	Version string `json:"version"`
}

type SysInfo struct {
	System  SystemInfo `json:"sys"`
	Network NetInfo    `json:"net"`
	Meta    MetaInfo   `json:"meta"`
}

func GetName() string {
	name, _ := os.Hostname()

	return name
}

func GetSystemInfo() SystemInfo {
	return SystemInfo{
		Hostname: GetName(),
		Os:       GetOS(),
		Edition:  GetEdition(),
		Arch:     GetArch(),
		Kernel:   GetKernel(),
		GUID:     GetGUID(),
		Type:     GetSystemType(),
		Locale:   GetLocaleName(),
	}
}

func GetNetworkInfo() NetInfo {
	nics := GetAllNics()
	var nic Nic

	// use the first NIC with a valid IPv4 address
	for _, nic = range nics {
		if nic.Ip.IsV4() {
			break
		}
	}

	return NetInfo{
		Nics: nics,
		Mac:  nic.Mac,
		Ip:   nic.Ip,
	}
}

func GetMetaInfo() MetaInfo {
	return MetaInfo{
		Version: VERSION,
	}
}

func GetAllInfo() SysInfo {
	return SysInfo{
		System:  GetSystemInfo(),
		Network: GetNetworkInfo(),
		Meta:    GetMetaInfo(),
	}
}
