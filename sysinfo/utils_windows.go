package sysinfo

import (
	"golang.org/x/sys/windows/registry"

	"errors"
	"fmt"
	"strings"
)

const (
	REG_SEP = `\`
)

/**
 * Return a handle to a registry hive based on some shortcut name for it.
 **/
func stringToHive(shortcut string) (hive registry.Key) {
	switch strings.ToUpper(shortcut) {
	case "HKCR":
		hive = registry.CLASSES_ROOT
	case "HKCU":
		hive = registry.CURRENT_USER
	case "HKLM":
		hive = registry.LOCAL_MACHINE
	case "HKU":
		hive = registry.USERS
	case "HKCC":
		hive = registry.CURRENT_CONFIG
	default:
		panic(fmt.Sprintf("Invalid hive: %s", shortcut))
	}

	return hive
}

/**
 * Split something like HKLM/Software/Microsoft/Windows into pieces like HKLM,
 * Software/Microsoft, and Windows.
 **/
func keyToRootAndKey(fullKey string) (hive registry.Key, root, key string, err error) {
	// turn sane slashes into derpy slashes
	key = strings.Replace(fullKey, "/", REG_SEP, -1)
	if strings.Count(key, REG_SEP) < 2 {
		return hive, root, key, errors.New(fmt.Sprintf("Invalid key: %s", fullKey))
	}

	hivePos := strings.Index(key, REG_SEP)
	keyPos := strings.LastIndex(key, REG_SEP)

	hive = stringToHive(key[:hivePos])
	root = key[hivePos+1 : keyPos]
	key = key[keyPos+1:]

	return hive, root, key, nil
}

/**
 * Helper function to get a string out of the Windows registry.
 **/
func GetRegString(key string, length uint32) (value string, err error) {
	hive, root, key, err := keyToRootAndKey(key)
	if err != nil {
		return value, err
	}

	reg, err := registry.OpenKey(hive, root, registry.READ|registry.WOW64_64KEY)
	if err != nil {
		return value, err
	}

	// make sure we clean up after ourselves
	defer reg.Close()

	// get the string value
	value, _, err = reg.GetStringValue(key)
	if err != nil {
		return value, err
	}

	return value, nil
}

// --------------- Helpers for commonly-used registry paths -----------------

func GetMicrosoft(key string, length uint32) (string, error) {
	return GetRegString("HKLM/SOFTWARE/Microsoft/"+key, length)
}

func GetWindowsNT(key string, length uint32) (string, error) {
	return GetMicrosoft("Windows NT/CurrentVersion/"+key, length)
}

func GetSysControl(key string, length uint32) (string, error) {
	return GetRegString("HKLM/System/CurrentControlSet/Control/"+key, length)
}
