package sysinfo

import (
	"bufio"
	"net"
	"os"
	"os/exec"
	"strings"
	"syscall"
)

var (
	os_files   = []string{"/etc/os-release", "/usr/lib/os-release", "/etc/redhat-release"}
	uts        syscall.Utsname
	knownInits = []string{"systemd", "upstart"}
)

/**
 * Load uname information before anything else
 **/
func InitPlatform() {
	syscall.Uname(&uts)
}

/**
 * Always return Linux as the OS
 **/
func GetOS() string {
	return "Linux"
}

/**
 * Attempt to determine what type of Linux system we're dealing with. If the
 * distro cannot be determined, just return "Linux".
 **/
func GetEdition() (name string) {
	name = "Linux"

	for _, path := range os_files {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			continue
		}

		file, err := os.Open(path)
		if err != nil {
			continue
		}
		defer file.Close()

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			line := scanner.Text()
			if strings.HasPrefix(line, "PRETTY_NAME=") {
				name = line[13 : len(line)-1]
				break
			} else if strings.HasPrefix(line, "NAME=") {
				name = line[6 : len(line)-1]
			} else if strings.Index(line, "=") == -1 {
				name = line
			}
		}

		break
	}

	return name
}

/**
 * Determin CPU architecture.
 **/
func GetArch() (arch string) {
	for _, c := range uts.Machine {
		if c == 0 {
			break
		}

		arch += string(byte(c))
	}

	return arch
}

/**
 * Determine kernel version.
 **/
func GetKernel() (kernel string) {
	for _, c := range uts.Release {
		if c == 0 {
			break
		}

		kernel += string(byte(c))
	}

	return kernel
}

/**
 * Get system UUID if possible.
 **/
func GetGUID() (guid string) {
	guid = "(must be run as root)"

	file, err := os.Open("/sys/devices/virtual/dmi/id/product_uuid")
	if err == nil {
		defer file.Close()

		scanner := bufio.NewScanner(file)
		scanner.Scan()
		guid = scanner.Text()
	} else {
		estr := err.Error()
		if strings.Index(estr, "no such file") != -1 {
			guid = "unknown"
		} else if strings.Index(estr, "permission denied") == -1 {
			guid = err.Error()
		}
	}

	return guid
}

/**
 * Attempt to determine whether this is a virtual or physical machine.
 **/
func GetSystemType() (systype string) {
	var guess string
	it := 0

	for {
		switch it {
		case 0:
			guess = sysTypeByVendor()
		case 1:
			guess = sysTypeByHypervisor()
		case 2:
			guess = sysTypeByCgroup()
		default:
			guess = "physical"
		}

		if len(guess) > 0 {
			return guess
		}

		it += 1
	}
}

/**
 * Check system type by vendor ID.
 **/
func sysTypeByVendor() (systype string) {
	CheckFirstLine("/sys/devices/virtual/dmi/id/sys_vendor", func(vendor string) bool {
		if IsVirtualBox(vendor) {
			systype = "VirtualBox"
		} else if IsVMware(vendor) {
			systype = "VMware"
		}

		return true
	})

	return systype
}

/**
 * Check system type by hypervisor type.
 **/
func sysTypeByHypervisor() (systype string) {
	CheckFirstLine("/sys/hypervisor/type", func(line string) bool {
		if len(line) > 0 {
			systype = line
		}

		return true
	})

	return systype
}

/**
 * Check to see if we're in a container.
 **/
func sysTypeByCgroup() (systype string) {
	CheckLines("/proc/1/cgroup", func(line string) bool {
		// non-containerized distros should have everything in /
		if !strings.HasSuffix(line, ":/") {
			if strings.Index(line, "docker") != -1 {
				// detect docker
				systype = "Docker"
				return true
			} else if strings.Index(line, "lxc") != -1 {
				// detect docker
				systype = "LXC"
				return true
			}
		}

		return false
	})

	return systype
}

/**
 * Return the name of a network interface.
 **/
func GetNicName(iface net.Interface) string {
	return iface.Name
}

// GetInitType returns the init system currently powering the linux box.
func GetInitType() string {
	var outStr string

	out, err := exec.Command("/proc/1/exe", "--version").Output()
	if err != nil {
		CheckFirstLine("/proc/1/comm", func(line string) bool {
			outStr = line
			return true
		})
	} else {
		outStr = string(out)
	}

	for _, name := range knownInits {
		if strings.Index(outStr, name) != -1 {
			return name
		}
	}

	return "unknown"
}

// IsUpstart returns true if it looks like the system uses upstart.
func IsUpstart() bool {
	return GetInitType() == "upstart"
}

// IsSystemd returns true if it looks like the system uses system.
func IsSystemd() bool {
	return GetInitType() == "systemd"
}
