package sysinfo

import (
	"testing"
)

func TestHasIdent(t *testing.T) {
	if !hasIdent("FooBarBaz", &[]string{"oba"}) {
		t.Error("hasIdent failed with one identity")
	}

	if !hasIdent("FooBarBaz", &[]string{"oa", "Ba"}) {
		t.Error("hasIdent failed with two identities")
	}

	if !hasIdent("FooBarBaz", &[]string{"oa", "BB", "BAZ"}) {
		t.Error("hasIdent failed with three identities")
	}
}

func TestIsVirtualBox(t *testing.T) {
	if !IsVirtualBox("VirtualBox") {
		t.Error("Failed to detect VirtualBox with caps")
	}

	if !IsVirtualBox("innotek GmbH") {
		t.Error("Failed to detect VirtualBox with innotek")
	}

	if IsVirtualBox("innotech") {
		t.Error("Erroneously detected VirtualBox with innotech")
	}
}

func TestIsVmware(t *testing.T) {
	if !IsVMware("VMware") {
		t.Error("Failed to detect VMware with caps")
	}
}

func TestExists(t *testing.T) {
	if !Exists([]string{"a", "b", "c"}, "b") {
		t.Error("Failed to detect item")
	}

	if Exists([]string{"a", "b", "c"}, "B") {
		t.Error("Detected item not in slice")
	}

	if Exists([]string{"a", "b", "c"}, "z") {
		t.Error("Detected item not in slice")
	}
}

func TestAppendIfMissing(t *testing.T) {
	slice := []string{}

	slice = AppendIfMissing(slice, "a")
	if len(slice) != 1 || slice[0] != "a" {
		t.Error("Failed to append item")
	}

	slice = AppendIfMissing(slice, "a")
	if len(slice) == 2 {
		t.Error("Appended the same item twice")
	}

	slice = AppendIfMissing(slice, "b")
	if len(slice) != 2 {
		t.Error("Failed to append item")
	}
}
