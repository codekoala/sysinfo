package sysinfo

import (
	"encoding/json"
	"net"
	"strconv"
	"strings"
)

var (
	private_ips = []string{
		//"10.0.0.0-10.255.255.255",
		//"192.168.0.0-192.168.255.255",
		"172.16.0.0-172.31.255.255",
		"169.254.0.0-169.254.255.255",
		"127.0.0.0-127.255.255.255",
	}
)

// Represents a function to check an IP address for validity
type validIP func(string) bool

// Simple type to be able to have methods tied to an IP
type Ip struct {
	ip string
}

type Nic struct {
	Name string `json:"name"`
	Ip   Ip     `json:"ip"`
	Mac  string `json:"mac"`
}

// Return true when ip appears to be a valid IPv4 address
func isIPv4(ip string) bool {
	if strings.Count(ip, ".") == 3 {
		o1, o2 := Ip{ip}.FirstOctets()

		// no valid IP has 0 as its first octet
		if o1 > 0 {
			// it's common for 10.x.x.x networks to use 10.0.x.x
			if o1 == 10 {
				return true
			} else if o1 != 10 && o2 > 0 {
				return true
			}
		}
	}

	return false
}

// Return true when ip appears to be a valid IPv6 address
func isIPv6(ip string) bool {
	if strings.Index(ip, ":") != -1 {
		if net.ParseIP(ip) != nil {
			return true
		}
	}

	return false
}

// Return true when ip appears to be a valid IPv4 or IPv6 address
func isIP(ip string) bool {
	return isIPv4(ip) || isIPv6(ip)
}

func (ip Ip) String() string {
	return ip.ip
}

func (ip Ip) MarshalJSON() ([]byte, error) {
	return json.Marshal(ip.ip)
}

func (ip Ip) IsV4() bool {
	return isIPv4(ip.ip)
}

func (ip Ip) IsV6() bool {
	return isIPv6(ip.ip)
}

func (ip Ip) IsValidIp() bool {
	return ip.IsV4() || ip.IsV6()
}

func (ip Ip) Octets() [4]uint8 {
	octets := [4]uint8{}

	for idx, octet := range strings.Split(ip.ip, ".") {
		value, _ := strconv.ParseInt(octet, 10, 0)
		octets[idx] = uint8(value)
	}

	return octets
}

// Return the first two octets of an IPv4 address as integers
func (ip Ip) FirstOctets() (uint8, uint8) {
	bits := ip.Octets()
	return bits[0], bits[1]
}

// Return true if Ip falls within the Ip range between begin and end
func (ip Ip) InRange(begin, end Ip) bool {
	bits := ip.Octets()
	bbits := begin.Octets()
	ebits := end.Octets()

	for idx := 0; idx < 4; idx++ {
		if bits[idx] < bbits[idx] || bits[idx] > ebits[idx] {
			return false
		}
	}

	return true
}

// Check to see if the IP address is a private network address that we don't
// care about
func (ip Ip) IsPrivate() bool {
	// TODO: Handle IPv6?
	if !ip.IsV4() {
		return false
	}

	for _, rng := range private_ips {
		ip_range := strings.Split(rng, "-")
		begin := Ip{ip_range[0]}
		end := Ip{ip_range[1]}

		if ip.InRange(begin, end) {
			return true
		}
	}

	return false
}

func GetNics(hasAddr validIP) (nics []Nic) {
	ifaces, _ := net.Interfaces()
	for _, iface := range ifaces {
		// skip loopback
		if string(iface.HardwareAddr) == "" {
			continue
		}

		addrs, _ := iface.Addrs()
		for _, addr := range addrs {
			ipStr := strings.Split(addr.String(), "/")[0]

			// determine if we have an IP address
			if !hasAddr(ipStr) {
				continue
			}

			// skip private IPs
			ip := Ip{ipStr}
			if ip.IsPrivate() {
				continue
			}

			nic := Nic{
				Name: GetNicName(iface),
				Ip:   ip,
				Mac:  iface.HardwareAddr.String(),
			}
			nics = append(nics, nic)
			break
		}
	}

	return nics
}

func getIPs(hasAddr validIP) (ips []string) {
	for _, nic := range GetNics(hasAddr) {
		ips = append(ips, nic.Ip.ip)
	}

	return ips
}

func GetIPv4() (ipv4 []string) {
	return getIPs(isIPv4)
}

func GetIPv6() (ipv4 []string) {
	return getIPs(isIPv4)
}

func GetAllNics() (nics []Nic) {
	return GetNics(isIP)
}

func GetAllIPs() (ips []string) {
	return getIPs(isIP)
}

func GetAllMacs() (macs []string) {
	for _, nic := range GetAllNics() {
		macs = AppendIfMissing(macs, nic.Mac)
	}

	return macs
}
