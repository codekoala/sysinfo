package sysinfo

import (
	"bytes"
	"strings"
	"os/exec"
)

var (
	vbox_ident   = []string{"virtualbox", "vbox", "innotek"}
	vmware_ident = []string{"vmware"}
)

func hasIdent(value string, idents *[]string) bool {
	value = strings.ToLower(value)

	for _, ident := range *idents {
		if strings.Index(value, strings.ToLower(ident)) != -1 {
			return true
		}
	}

	return false
}

/**
 * Determine when some string matches identifiers for Oracle VirtualBox.
 **/
func IsVirtualBox(value string) bool {
	return hasIdent(value, &vbox_ident)
}

/**
 * Determine when some string matches identifiers for VMware.
 **/
func IsVMware(value string) bool {
	return hasIdent(value, &vmware_ident)
}

func Exists(slice []string, newItem string) bool {
	for _, item := range slice {
		if item == newItem {
			return true
		}
	}

	return false
}

func AppendIfMissing(slice []string, newItem string) []string {
	if !Exists(slice, newItem) {
		slice = append(slice, newItem)
	}

	return slice
}

func getOutput(args ...string) string {
	var out bytes.Buffer

	cmd := exec.Command(args[0], args[1:]...)
	cmd.Stdout = &out
	cmd.Run()

	return strings.TrimSpace(out.String())
}
