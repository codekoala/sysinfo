LDFLAGS := "-s"
all: test clean win linux linux-arm

clean:
	mkdir -p bin
	rm -rf bin/*

deps-win:
	GOOS=windows GOARCH=386 go get -u golang.org/x/sys/windows/registry

win:
	GOOS=windows GOARCH=386 go build -ldflags $(LDFLAGS) -o bin/getinfo.exe getinfo.go

linux:
	GOARCH=386 go build -ldflags $(LDFLAGS) -o bin/getinfo getinfo.go

linux-arm:
	GOARCH=arm go build -ldflags $(LDFLAGS) -o bin/getinfo.arm getinfo.go

upx:
	upx bin/*

test:
	go test ./sysinfo
