.. image:: https://drone.io/bitbucket.org/codekoala/sysinfo/status.png
   :target: https://drone.io/bitbucket.org/codekoala/sysinfo/latest
   
System Information
==================

This is a simple utility designed to print various bits of interesting system information.